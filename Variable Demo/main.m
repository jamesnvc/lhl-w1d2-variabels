//
//  main.m
//  Variable Demo
//
//  Created by James Cash on 06-03-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSInteger numberOfStudents = 8;

        NSLog(@"We have %ld students here", numberOfStudents);

        numberOfStudents = 9;

        NSInteger whateverVariable;

        NSLog(@"We just made whateverVar with %ld in it", whateverVariable);

        whateverVariable = numberOfStudents;

        NSLog(@"now whateverVariable has %ld in it", whateverVariable);

        whateverVariable = 17;
        NSLog(@"Whatever is %ld and students is %ld", whateverVariable, numberOfStudents);

        NSDate *myDate = [[NSDate alloc] init];

        NSLog(@"Date is %@", myDate);

        NSDate *anotherDate = myDate;

        NSLog(@"Other date is %@", anotherDate);

        anotherDate = [[NSDate alloc] init];

        NSLog(@"First date is %@ & second is %@", myDate, anotherDate);

        NSMutableString *stringOne = [NSMutableString stringWithString:@"Hello"];
        NSMutableString *stringTwo = stringOne;

        NSLog(@"1 = %@, 2 = %@", stringOne, stringTwo);

        [stringTwo appendString:@", world!"];

        NSLog(@"1 = %@, 2 = %@", stringOne, stringTwo);

        // Scopes
        {
            BOOL countIsGreaterThanTen = NO;
            NSInteger count = 11;
            if (count > 10) {
                countIsGreaterThanTen = YES;
            }

            if (countIsGreaterThanTen) {
                NSLog(@"The count really is more than ten");
            }
            countIsGreaterThanTen;
        }
        // ERROR:
//        countIsGreaterThanTen;

        NSLog(@"💩 \" ");
    }
    return 0;
}
